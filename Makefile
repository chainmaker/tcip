all: pb rpc

pb:
	protoc -I=. --gogofaster_out=:../tcip-go --gogofaster_opt=paths=source_relative common/*.proto
	protoc -I=. --gogofaster_out=:../tcip-go --gogofaster_opt=paths=source_relative common/cross_chain/*.proto
	protoc -I=. --gogofaster_out=:../tcip-go --gogofaster_opt=paths=source_relative common/relay_chain/*.proto

rpc:
	protoc -I=. \
		-I=${GOPATH}/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.16.0/third_party/googleapis \
		--gogofaster_out=plugins=grpc:../tcip-go \
		--gogofaster_opt=paths=source_relative \
		--grpc-gateway_opt=paths=source_relative \
		--grpc-gateway_out=logtostderr=true:../tcip-go \
		--swagger_out=logtostderr=true:../tcip-go \
		api/*.proto

pb-dep:
	go get -u google.golang.org/grpc/cmd/protoc-gen-go-grpc
	go get -u github.com/golang/protobuf/protoc-gen-go
	go get -u google.golang.org/grpc
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go get -u github.com/gogo/protobuf/protoc-gen-gogofaster

docs-dep:
	go get -u github.com/pseudomuto/protoc-gen-doc/cmd/protoc-gen-doc

docs:
	protoc \
    	-I=${GOPATH}/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.16.0/third_party/googleapis \
    	--plugin=protoc-gen-doc=${GOPATH}/bin/protoc-gen-doc \
    	--doc_out=./doc \
    	--proto_path=. \
      	--doc_opt=markdown,index.md \
    	*/*.proto */*/*.proto