# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [api/rpc_cross_chain.proto](#api_rpc_cross_chain-proto)
    - [RpcCrossChain](#api-RpcCrossChain)
  
- [api/rpc_relay_chain.proto](#api_rpc_relay_chain-proto)
    - [RpcRelayChain](#api-RpcRelayChain)
  
- [common/common.proto](#common_common-proto)
    - [BcosConfig](#common-BcosConfig)
    - [CancelInfo](#common-CancelInfo)
    - [ChainmakerConfig](#common-ChainmakerConfig)
    - [ConfirmInfo](#common-ConfirmInfo)
    - [ContractKeyValuePair](#common-ContractKeyValuePair)
    - [CrossChainConfirm](#common-CrossChainConfirm)
    - [CrossChainConfirmUpChain](#common-CrossChainConfirmUpChain)
    - [CrossChainEvent](#common-CrossChainEvent)
    - [CrossChainInfo](#common-CrossChainInfo)
    - [CrossChainMsg](#common-CrossChainMsg)
    - [CrossChainTxUpChain](#common-CrossChainTxUpChain)
    - [FabricConfig](#common-FabricConfig)
    - [GatewayInfo](#common-GatewayInfo)
    - [MapEventDataType](#common-MapEventDataType)
    - [Node](#common-Node)
    - [Orderer](#common-Orderer)
    - [Org](#common-Org)
    - [PageInfo](#common-PageInfo)
    - [Peer](#common-Peer)
    - [Pkcs11](#common-Pkcs11)
    - [TxContent](#common-TxContent)
    - [TxContentWithVerify](#common-TxContentWithVerify)
    - [TxVerifyInterface](#common-TxVerifyInterface)
  
    - [CallType](#common-CallType)
    - [ChainmakerRuntimeType](#common-ChainmakerRuntimeType)
    - [Code](#common-Code)
    - [CrossChainStateValue](#common-CrossChainStateValue)
    - [CrossType](#common-CrossType)
    - [EventDataType](#common-EventDataType)
    - [EventName](#common-EventName)
    - [Operate](#common-Operate)
    - [TriggerCondition](#common-TriggerCondition)
    - [TryResult](#common-TryResult)
    - [TxResultValue](#common-TxResultValue)
    - [TxVerifyRsult](#common-TxVerifyRsult)
    - [TxVerifyType](#common-TxVerifyType)
    - [Version](#common-Version)
  
- [common/cross_chain/request.proto](#common_cross_chain_request-proto)
    - [ChainIdentityRequest](#cross_chain-ChainIdentityRequest)
    - [CrossChainCancelRequest](#cross_chain-CrossChainCancelRequest)
    - [CrossChainConfirmRequest](#cross_chain-CrossChainConfirmRequest)
    - [CrossChainEventRequest](#cross_chain-CrossChainEventRequest)
    - [CrossChainTryRequest](#cross_chain-CrossChainTryRequest)
    - [IsCrossChainSuccessRequest](#cross_chain-IsCrossChainSuccessRequest)
    - [TxVerifyRequest](#cross_chain-TxVerifyRequest)
  
- [common/cross_chain/response.proto](#common_cross_chain_response-proto)
    - [ChainIdentityResponse](#cross_chain-ChainIdentityResponse)
    - [CrossChainCancelResponse](#cross_chain-CrossChainCancelResponse)
    - [CrossChainConfirmResponse](#cross_chain-CrossChainConfirmResponse)
    - [CrossChainEventResponse](#cross_chain-CrossChainEventResponse)
    - [CrossChainTryResponse](#cross_chain-CrossChainTryResponse)
    - [IsCrossChainSuccessResponse](#cross_chain-IsCrossChainSuccessResponse)
    - [PingPongResponse](#cross_chain-PingPongResponse)
    - [TxVerifyResponse](#cross_chain-TxVerifyResponse)
  
- [common/relay_chain/request.proto](#common_relay_chain_request-proto)
    - [BeginCrossChainRequest](#relay_chain-BeginCrossChainRequest)
    - [GatewayRegisterRequest](#relay_chain-GatewayRegisterRequest)
    - [GatewayUpdateRequest](#relay_chain-GatewayUpdateRequest)
    - [InitContractRequest](#relay_chain-InitContractRequest)
    - [QueryCrossChainRequest](#relay_chain-QueryCrossChainRequest)
    - [QueryGatewayRequest](#relay_chain-QueryGatewayRequest)
    - [SyncBlockHeaderRequest](#relay_chain-SyncBlockHeaderRequest)
    - [UpdateContractRequest](#relay_chain-UpdateContractRequest)
  
- [common/relay_chain/response.proto](#common_relay_chain_response-proto)
    - [BeginCrossChainResponse](#relay_chain-BeginCrossChainResponse)
    - [GatewayRegisterResponse](#relay_chain-GatewayRegisterResponse)
    - [GatewayUpdateResponse](#relay_chain-GatewayUpdateResponse)
    - [InitContractResponse](#relay_chain-InitContractResponse)
    - [QueryCrossChainResponse](#relay_chain-QueryCrossChainResponse)
    - [QueryGatewayResponse](#relay_chain-QueryGatewayResponse)
    - [SyncBlockHeaderResponse](#relay_chain-SyncBlockHeaderResponse)
    - [UpdateContractResponse](#relay_chain-UpdateContractResponse)
  
- [Scalar Value Types](#scalar-value-types)



<a name="api_rpc_cross_chain-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## api/rpc_cross_chain.proto
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0

 

 

 


<a name="api-RpcCrossChain"></a>

### RpcCrossChain
跨链网关api

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| CrossChainTry | [.cross_chain.CrossChainTryRequest](#cross_chain-CrossChainTryRequest) | [.cross_chain.CrossChainTryResponse](#cross_chain-CrossChainTryResponse) | 接收跨链请求的接口 |
| CrossChainConfirm | [.cross_chain.CrossChainConfirmRequest](#cross_chain-CrossChainConfirmRequest) | [.cross_chain.CrossChainConfirmResponse](#cross_chain-CrossChainConfirmResponse) | 跨链结果确认，提交 |
| CrossChainCancel | [.cross_chain.CrossChainCancelRequest](#cross_chain-CrossChainCancelRequest) | [.cross_chain.CrossChainCancelResponse](#cross_chain-CrossChainCancelResponse) | 跨链结果确认，回滚 |
| IsCrossChainSuccess | [.cross_chain.IsCrossChainSuccessRequest](#cross_chain-IsCrossChainSuccessRequest) | [.cross_chain.IsCrossChainSuccessResponse](#cross_chain-IsCrossChainSuccessResponse) | 判断跨链结果 |
| TxVerify | [.cross_chain.TxVerifyRequest](#cross_chain-TxVerifyRequest) | [.cross_chain.TxVerifyResponse](#cross_chain-TxVerifyResponse) | rpc交易验证，不是非要在当前服务中实现 |
| CrossChainEvent | [.cross_chain.CrossChainEventRequest](#cross_chain-CrossChainEventRequest) | [.cross_chain.CrossChainEventResponse](#cross_chain-CrossChainEventResponse) | 跨链触发器管理 |
| ChainIdentity | [.cross_chain.ChainIdentityRequest](#cross_chain-ChainIdentityRequest) | [.cross_chain.ChainIdentityResponse](#cross_chain-ChainIdentityResponse) | 链身份管理 |
| PingPong | [.google.protobuf.Empty](#google-protobuf-Empty) | [.cross_chain.PingPongResponse](#cross_chain-PingPongResponse) | 心跳检测 |

 



<a name="api_rpc_relay_chain-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## api/rpc_relay_chain.proto
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0

 

 

 


<a name="api-RpcRelayChain"></a>

### RpcRelayChain
中继网关api

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| SyncBlockHeader | [.relay_chain.SyncBlockHeaderRequest](#relay_chain-SyncBlockHeaderRequest) | [.relay_chain.SyncBlockHeaderResponse](#relay_chain-SyncBlockHeaderResponse) | 同步区块头 |
| BeginCrossChain | [.relay_chain.BeginCrossChainRequest](#relay_chain-BeginCrossChainRequest) | [.relay_chain.BeginCrossChainResponse](#relay_chain-BeginCrossChainResponse) | 接收跨链请求 |
| GatewayRegister | [.relay_chain.GatewayRegisterRequest](#relay_chain-GatewayRegisterRequest) | [.relay_chain.GatewayRegisterResponse](#relay_chain-GatewayRegisterResponse) | 网关注册 |
| GatewayUpdate | [.relay_chain.GatewayUpdateRequest](#relay_chain-GatewayUpdateRequest) | [.relay_chain.GatewayUpdateResponse](#relay_chain-GatewayUpdateResponse) | 网关信息更新 |
| InitContract | [.relay_chain.InitContractRequest](#relay_chain-InitContractRequest) | [.relay_chain.InitContractResponse](#relay_chain-InitContractResponse) | 用于具有同步区块头、交易验证功能的spv合约的创建 |
| UpdateContract | [.relay_chain.UpdateContractRequest](#relay_chain-UpdateContractRequest) | [.relay_chain.UpdateContractResponse](#relay_chain-UpdateContractResponse) | 用于具有同步区块头、交易验证功能的spv合约的更新 |
| QueryGateway | [.relay_chain.QueryGatewayRequest](#relay_chain-QueryGatewayRequest) | [.relay_chain.QueryGatewayResponse](#relay_chain-QueryGatewayResponse) | 网关信息查询 |
| QueryCrossChain | [.relay_chain.QueryCrossChainRequest](#relay_chain-QueryCrossChainRequest) | [.relay_chain.QueryCrossChainResponse](#relay_chain-QueryCrossChainResponse) | 跨链信息查询，暂不支持通过业务链交易id查询跨链交易，用户需要自行维护业务链的交易id与跨链交易id的映射关系 |

 



<a name="common_common-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## common/common.proto
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0


<a name="common-BcosConfig"></a>

### BcosConfig
bcos配置信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| chain_rid | [string](#string) |  | 链资源Id |
| ca | [string](#string) |  | ca |
| tls_key | [string](#string) |  | key |
| tls_cert | [string](#string) |  | cert |
| private_key | [string](#string) |  | private key |
| group_id | [string](#string) |  | group id |
| address | [string](#string) |  | 地址 |
| chain_id | [int64](#int64) |  | chain id |
| http | [bool](#bool) |  | http |
| is_sm_crypto | [bool](#bool) |  | sm |
| state | [bool](#bool) |  | 状态，由网关设置用户不需要关系 |
| state_message | [string](#string) |  | 状态描述，由网关设置，状态为true，此处是成功信息，状态为false，此处有错误信息 |






<a name="common-CancelInfo"></a>

### CancelInfo
cancel数据


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| chain_rid | [string](#string) |  | 链资源Id |
| contract_name | [string](#string) |  | 合约名称 |
| method | [string](#string) |  | 合约方法名称 |
| parameter | [string](#string) |  | 合约参数，json字符串，chainmaker为kv string json字符串，fabric为[]string json字符串，bcos为[]string json字符串 QUERY类型的跨链交易回调源网关的时候，失败不需要回写，不需要“TRY_RESULT”占位符，编写合约的时候需要注意参数设置 |
| param_data | [int32](#int32) | repeated | 参数数据在eventData数组中的索引，基于该字段生成的数组将被使用fmt.Sprintf方法与parameter参数共同组成目标合约参数，只用于交易触发模版 eventData是合约事件的具体内容生成的数组，chainmaker为具体的事件内容数组，fabric为具体时间内容经过string、map[string]string、[]string处理之后生成的数组 |
| param_data_type | [EventDataType](#common-EventDataType) | repeated | bcos 需要指定每个参数的类型，QUERY类型的跨链交易失败不需要回写，不需要添加try_result的类型 |
| extra_data | [string](#string) |  | 扩展参数 |
| abi | [string](#string) |  | bcos abi |






<a name="common-ChainmakerConfig"></a>

### ChainmakerConfig
chainmaker配置信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| chain_rid | [string](#string) |  | 链资源Id |
| chain_id | [string](#string) |  | 链Id |
| org_id | [string](#string) |  | 组织id |
| sign_cert | [string](#string) |  | 签名证书 |
| sign_key | [string](#string) |  | 签名私钥 |
| tls_cert | [string](#string) |  | tls证书 |
| tls_key | [string](#string) |  | tls私钥 |
| auth_type | [string](#string) |  | chainmaker证书模式，保留字段，先支持证书模式，后续再支持其他模式 |
| node | [Node](#common-Node) | repeated | 节点 |
| pkcs11 | [Pkcs11](#common-Pkcs11) |  | pkcs11 保留字段，先支持证书模式，后续再支持其他模式 |
| state | [bool](#bool) |  | 状态，由网关设置用户不需要关心 |
| state_message | [string](#string) |  | 状态描述，由网关设置，状态为true，此处是成功信息，状态为false，此处有错误信息 |






<a name="common-ConfirmInfo"></a>

### ConfirmInfo
confirm数据


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| chain_rid | [string](#string) |  | 链资源Id |
| contract_name | [string](#string) |  | 合约名称 |
| method | [string](#string) |  | 合约方法名称 |
| parameter | [string](#string) |  | 合约参数，json字符串，chainmaker为kv string json字符串，fabric为[]string json字符串，bcos为[]string json字符串 QUERY类型的跨链交易回调源网关的时候，添加“TRY_RESULT”占位符会将跨链交易的目标链返回值按照发起顺序替换掉，其他confirm流程不会触发，如&#34;{\&#34;key\&#34;:\&#34;%s\&#34;,\&#34;value\&#34;:\&#34;TRY_RESULT\&#34;&#34;}&#34;或&#34;[\&#34;%s\&#34;,&#34;TRY_RESULT&#34;]&#34; 占位符数量要和跨链交易数量相同，新建跨链event的时候会进行检测 |
| param_data | [int32](#int32) | repeated | 参数数据在eventData数组中的索引，基于该字段生成的数组将被使用fmt.Sprintf方法与parameter参数共同组成目标合约参数，只用于交易触发模版 eventData是合约事件的具体内容生成的数组，chainmaker为具体的事件内容，fabric为具体时间内容经过string、map[string]string、[]string处理之后生成的数组 |
| param_data_type | [EventDataType](#common-EventDataType) | repeated | bcos 需要指定每个参数的类型，QUERY类型的跨链交易会将try_result值附加在后面，需要加上try_result的类型，合约的编写的时候需要注意参数设置 |
| extra_data | [string](#string) |  | 扩展参数 |
| abi | [string](#string) |  | bcos abi，只有bcos需要 |






<a name="common-ContractKeyValuePair"></a>

### ContractKeyValuePair
合约参数，k-v键值对


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  | 键 |
| value | [bytes](#bytes) |  | 值 |






<a name="common-CrossChainConfirm"></a>

### CrossChainConfirm
各网关确认结果


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| code | [Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |






<a name="common-CrossChainConfirmUpChain"></a>

### CrossChainConfirmUpChain
网关确认结果上链对象


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| index | [int32](#int32) |  | 确认的索引 |
| cross_chain_confirm | [CrossChainConfirm](#common-CrossChainConfirm) |  | 确认结果 |






<a name="common-CrossChainEvent"></a>

### CrossChainEvent
跨链触发器


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| cross_chain_event_id | [string](#string) |  | 唯一ID，组成为“event_name#contract_name#chain_rid”，不需要用户设置 |
| chain_rid | [string](#string) |  | chain_rid链资源id |
| contract_name | [string](#string) |  | 合约名称， bcos为地址 |
| cross_chain_name | [string](#string) |  | 跨链名称 |
| cross_chain_flag | [string](#string) |  | 跨链标记 |
| trigger_condition | [TriggerCondition](#common-TriggerCondition) |  | 触发方式 |
| event_name | [string](#string) |  | 事件名称， bcos为event，如 SetState(uint256,string) |
| fabric_event_data_type | [EventDataType](#common-EventDataType) |  | fabric事件数据类型，只支持string、map、array string类型会生成只有一个元素的eventData数组 map类型会按照map[string]string反序列化，生成和MapEventDataType中key值对应顺序的eventData数组 array会按照[]string反序列化 |
| fabric_map_event_data_type | [MapEventDataType](#common-MapEventDataType) | repeated | fabric map事件数据类型，key的填写顺序会映射为eventData数组，跨链消息中的所有param_data字段需要填写为该数组字段的索引 |
| fabric_array_event_data_type | [EventDataType](#common-EventDataType) | repeated | fabric array事件数据类型， |
| chainmaker_event_data_type | [EventDataType](#common-EventDataType) | repeated | chainmaker事件数据类型，eventData数组即事件的数组 |
| bcos_event_data_type | [EventDataType](#common-EventDataType) | repeated | fisco bcos 事件数据类型，这里会将topic中的数据一并计算进去，并且由topic的数据开头，生成eventData数组 |
| abi | [string](#string) | repeated | fisco bcos 事件abi，即事件中每个参数的abi，这里写错会导致网关无法接些event的具体内容 |
| is_cross_chain | [string](#string) |  | 是否为跨链交易，bcos、chainmaker、fabirc eventData数组的字段名称需要写成 &#34;zero&#34;, &#34;one&#34;, &#34;two&#34;, &#34;three&#34;, &#34;four&#34;, &#34;five&#34;, &#34;six&#34;, &#34;seven&#34;, &#34;eight&#34;, &#34;nine&#34;, &#34;ten&#34;这种形式，目前只支持到10，扩展只需要简单的修改代码即可 map的字段名称照实写就可以了 例如 &#34;one == \&#34;lockaddress\&#34;&#34; bcos地址如果类型写为address，请务必实测fisco bcos go sdk监听到的具体字符串之后再填写 （判断字符串,参考https://github.com/Knetic/govaluate） |
| confirm_info | [ConfirmInfo](#common-ConfirmInfo) |  | 作为标识，中继网关向跨链网关发送确认操作 |
| cancel_info | [CancelInfo](#common-CancelInfo) |  | 作为标识，中继网关向跨链网关发送取消操作 |
| cross_chain_create | [CrossChainMsg](#common-CrossChainMsg) | repeated | 目标网关id |
| timeout | [int64](#int64) |  | 超时时间 s |
| cross_type | [CrossType](#common-CrossType) |  | 跨链类型 |
| state | [bool](#bool) |  | 状态，由网关设置用户不需要关心 |
| state_message | [string](#string) |  | 状态描述，由网关设置，状态为true，此处是成功信息，状态为false，此处有错误信息 |






<a name="common-CrossChainInfo"></a>

### CrossChainInfo
跨链信息在链上的存储结构


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| cross_chain_id | [string](#string) |  | 跨链Id |
| cross_chain_name | [string](#string) |  | 跨链名称 |
| cross_chain_flag | [string](#string) |  | 跨链标记 |
| from | [string](#string) |  | 发起跨链网关的id |
| cross_chain_msg | [CrossChainMsg](#common-CrossChainMsg) | repeated | 跨链信息具体内容，该数组的下标和cross_chain_tx_content/gateway_confirm_result一一对应 |
| first_tx_content | [TxContentWithVerify](#common-TxContentWithVerify) |  | 跨链首笔交易内容 |
| cross_chain_tx_content | [TxContentWithVerify](#common-TxContentWithVerify) | repeated | 跨链交易的内容，包含各网关的交易结果 |
| cross_chain_result | [bool](#bool) |  | 跨链结果 |
| gateway_confirm_result | [CrossChainConfirm](#common-CrossChainConfirm) | repeated | 各网关确认结果 |
| state | [CrossChainStateValue](#common-CrossChainStateValue) |  | 跨链状态 |
| confirm_info | [ConfirmInfo](#common-ConfirmInfo) |  | 源网关confirm数据，作为标识，中继网关向跨链网关发送确认操作 |
| cancel_info | [CancelInfo](#common-CancelInfo) |  | 源网关cancel数据，作为标识，中继网关向跨链网关发送取消操作 |
| confirm_result | [CrossChainConfirm](#common-CrossChainConfirm) |  | 源网关确认结果 |
| timeout | [int64](#int64) |  | 超时时间 s |
| cross_type | [CrossType](#common-CrossType) |  | 跨链类型 |






<a name="common-CrossChainMsg"></a>

### CrossChainMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| gateway_id | [string](#string) |  | 目标网关Id |
| chain_rid | [string](#string) |  | 目标链资源id |
| contract_name | [string](#string) |  | 目标链合约名称 |
| method | [string](#string) |  | 目标合约方法名称 |
| identity | [string](#string) | repeated | 签名身份Id |
| parameter | [string](#string) |  | 目标合约参数，json字符串，chainmaker为kv string json字符串，fabric为array string json字符串，bcos为array string json字符串 |
| param_data | [int32](#int32) | repeated | 参数数据在eventData数组中的索引，基于该字段生成的数组将被使用fmt.Sprintf方法与parameter参数共同组成目标合约参数，只用于交易触发模版 eventData是合约事件的具体内容生成的数组，chainmaker为具体的事件内容，fabric为具体时间内容经过string、map[string]string、[]string处理之后生成的数组 |
| param_data_type | [EventDataType](#common-EventDataType) | repeated | bcos 需要指定每个参数的类型，其他不需要 |
| extra_data | [string](#string) |  | 扩展数据，由发起网关和目标网关商定，中继网关只负责传输 |
| confirm_info | [ConfirmInfo](#common-ConfirmInfo) |  | QUERY类型指定无效，作为标识，中继网关向跨链网关发送确认操作 |
| cancel_info | [CancelInfo](#common-CancelInfo) |  | QUERY类型指定无效， 作为标识，中继网关向跨链网关发送取消操作 |
| abi | [string](#string) |  | bcos abi，只有bcos需要 |






<a name="common-CrossChainTxUpChain"></a>

### CrossChainTxUpChain
网关交易结果上链对象


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| index | [int32](#int32) |  | 确认的索引 |
| tx_content_with_verify | [TxContentWithVerify](#common-TxContentWithVerify) |  | 交易结果 |






<a name="common-FabricConfig"></a>

### FabricConfig
fabric配置信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| chain_rid | [string](#string) |  | 链资源Id |
| chain_id | [string](#string) |  | 链Id |
| org | [Org](#common-Org) | repeated | 组织 |
| tls_cert | [string](#string) |  | tls证书 |
| tls_key | [string](#string) |  | tls私钥 |
| orderers | [Orderer](#common-Orderer) | repeated | orderer节点 |
| state | [bool](#bool) |  | 状态，由网关设置用户不需要关系 |
| state_message | [string](#string) |  | 状态描述，由网关设置，状态为true，此处是成功信息，状态为false，此处有错误信息 |






<a name="common-GatewayInfo"></a>

### GatewayInfo
网关信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| gateway_id | [string](#string) |  | 网关id，注册的时候不需要写，由中继网关生成，保证唯一性 |
| gateway_name | [string](#string) |  | 网关名称，客户自定义，也可以没有，只是一个标记，没有实际意义 |
| address | [string](#string) |  | 网关地址 |
| server_name | [string](#string) |  | 网关的servername |
| tlsca | [string](#string) |  | 网关tlsca证书 |
| client_cert | [string](#string) |  | 网关客户端证书 |
| to_gateway_list | [string](#string) | repeated | 允许向哪些网关发起跨链请求,空表示不作限制 |
| from_gateway_list | [string](#string) | repeated | 允许哪些网关向它发起跨链请求，空表示不作限制 |
| tx_verify_type | [TxVerifyType](#common-TxVerifyType) |  | 交易验证类型 |
| TxVerifyInterface | [TxVerifyInterface](#common-TxVerifyInterface) |  | 交易验证接口,先支持restful |
| call_type | [CallType](#common-CallType) |  | 调用类型 |
| relay_chain_id | [string](#string) |  | 该跨链网关在注册于哪个中继网关 |






<a name="common-MapEventDataType"></a>

### MapEventDataType
map类型的事件类型


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  | key |
| data_type | [EventDataType](#common-EventDataType) |  | 数据类型 |






<a name="common-Node"></a>

### Node
chainmaker节点信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| node_addr | [string](#string) |  | 节点地址 |
| conn_cnt | [int32](#int32) |  | 连接池大小 |
| enable_tls | [bool](#bool) |  | 是否开启tls |
| trust_root | [string](#string) | repeated | trust_root_paths |
| tls_host_name | [string](#string) |  | tls_host_name |






<a name="common-Orderer"></a>

### Orderer
fabric orderer 节点信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| node_addr | [string](#string) |  | 节点地址 |
| trust_root | [string](#string) | repeated | trust_root_paths |
| tls_host_name | [string](#string) |  | tls_host_name |






<a name="common-Org"></a>

### Org
fabric org 信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| org_id | [string](#string) |  | org id |
| msp_id | [string](#string) |  | mspId |
| sign_cert | [string](#string) |  | 签名证书 |
| sign_key | [string](#string) |  | 签名私钥 |
| peers | [Peer](#common-Peer) | repeated | peer节点 |






<a name="common-PageInfo"></a>

### PageInfo
分页信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| page_size | [uint64](#uint64) |  | 每页多少数据 |
| page_number | [uint64](#uint64) |  | 第几页 |
| total_count | [uint64](#uint64) |  | 总共有多少数据 |
| limit | [uint64](#uint64) |  | 总共有多少页 |






<a name="common-Peer"></a>

### Peer
fabric peer 节点信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| node_addr | [string](#string) |  | 节点地址 |
| trust_root | [string](#string) | repeated | trust_root_paths |
| tls_host_name | [string](#string) |  | tls_host_name |






<a name="common-Pkcs11"></a>

### Pkcs11
pkcs11 密码机相关


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| enable | [bool](#bool) |  | 是否开启，现在一律关闭不做处理 |






<a name="common-TxContent"></a>

### TxContent
交易内容


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tx_id | [string](#string) |  | 交易id |
| tx | [bytes](#bytes) |  | 交易实际内容，json字符串 |
| tx_result | [TxResultValue](#common-TxResultValue) |  | 交易执行结果 |
| gateway_id | [string](#string) |  | 执行该交易的网关Id |
| chain_rid | [string](#string) |  | 执行该交易的链资源Id |
| tx_prove | [string](#string) |  | json字符串,这里是验证的参数，不管什么认证方式都会直接传给spv或者认证接口 |
| block_height | [int64](#int64) |  | 在哪个块中 |






<a name="common-TxContentWithVerify"></a>

### TxContentWithVerify
交易内容，包括验证结果


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tx_content | [TxContent](#common-TxContent) |  | 交易的具体结果和内容 |
| try_result | [string](#string) | repeated | try result |
| tx_verify_result | [TxVerifyRsult](#common-TxVerifyRsult) |  | 中继网关对交易的认证结果 |






<a name="common-TxVerifyInterface"></a>

### TxVerifyInterface
交易验证接口，先支持restful


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| address | [string](#string) |  | address,如，不需要写http或者https，127.0.0.1:8080/txverify |
| tls_enable | [bool](#bool) |  | 是否开启tls |
| tlsca | [string](#string) |  | 交易验证接口tlsca证书 |
| client_cert | [string](#string) |  | 交易验证接口客户端证书 |
| host_name | [string](#string) |  | 服务名 |





 


<a name="common-CallType"></a>

### CallType
网关调用类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| REST | 0 | restful接口方式 |
| GRPC | 1 | grpc方式 |



<a name="common-ChainmakerRuntimeType"></a>

### ChainmakerRuntimeType
合约运行时

| Name | Number | Description |
| ---- | ------ | ----------- |
| INVALID | 0 | 无效的 |
| NATIVE | 1 | 长安链内部系统合约 |
| WASMER | 2 | vm-wasmer, language-c&#43;&#43; |
| WXVM | 3 | vm-wxvm, language-cpp |
| GASM | 4 | wasm, tinygo |
| EVM | 5 | vm-evm |
| DOCKER_GO | 6 | vm-docker, go语言 |
| DOCKER_JAVA | 7 | vm-docker, java语言 |



<a name="common-Code"></a>

### Code


| Name | Number | Description |
| ---- | ------ | ----------- |
| GATEWAY_SUCCESS | 0 | 网关返回成功 |
| GATEWAY_TIMEOUT | 1 | 网关超时 |
| INVALID_PARAMETER | 2 | 参数无效 |
| TX_PROVE_ERROR | 3 | 交易认证失败 |
| CONTRACT_FAIL | 4 | 合约执行失败 |
| INTERNAL_ERROR | 5 | 内部错误 |
| RELAY_CHAIN_ERROR | 6 | 中继链错误 |



<a name="common-CrossChainStateValue"></a>

### CrossChainStateValue
跨链状态

| Name | Number | Description |
| ---- | ------ | ----------- |
| NEW | 0 | 新建 |
| WAIT_EXECUTE | 1 | 待执行 |
| WAIT_CONFIRM | 2 | 待提交 |
| CONFIRM_END | 3 | 确认结束 |
| CANCEL_END | 4 | 回滚结束 |



<a name="common-CrossType"></a>

### CrossType


| Name | Number | Description |
| ---- | ------ | ----------- |
| QUERY | 0 | 跨链查询 |
| INVOKE | 1 | 跨链交易 |



<a name="common-EventDataType"></a>

### EventDataType
事件数据类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| STRING | 0 | string |
| MAP | 1 | map |
| BYTE | 2 | byte |
| BOOL | 3 | BOOL |
| INT | 4 | int |
| FLOAT | 5 | float |
| ARRAY | 6 | 数组 |
| HASH | 7 | hash, bcos专用 |
| ADDRESS | 8 | ADDRESS, bcos专用 |



<a name="common-EventName"></a>

### EventName


| Name | Number | Description |
| ---- | ------ | ----------- |
| NEW_CROSS_CHAIN | 0 | new |
| CROSS_CHAIN_TRY_END | 1 | try end |
| UPADATE_RESULT_END | 2 | 跨链结果更新完成 |
| GATEWAY_CONFIRM_END | 3 | 提交完成 |
| SRC_GATEWAY_CONFIRM_END | 4 | 源网关提交完成 |



<a name="common-Operate"></a>

### Operate
跨链事件管理操作

| Name | Number | Description |
| ---- | ------ | ----------- |
| SAVE | 0 | 新建 |
| UPDATE | 1 | 更新 |
| DELETE | 2 | 删除 |
| GET | 3 | 获取 |



<a name="common-TriggerCondition"></a>

### TriggerCondition
跨链触发方式

| Name | Number | Description |
| ---- | ------ | ----------- |
| COMPLETELY_CONTRACT_EVENT | 0 | 完全用户合约事件触发，用户需要将relaye_chain.BeginCrossChainRequest结构自行在合约中组装，通过pb序列化后base64编码用事件发送出来（包含跨链数据,不需要其他信息） chainmaker为事件数组第一个，fabric为数组payload，bcos为事件第一个参数 |
| CONTRACT_EVENT | 1 | 用户合约事件触发（不包含跨链数据，需要跨链触发器提供其他信息） |
| TBIS_CONTRACT | 2 | TBIS协议合约适配 |



<a name="common-TryResult"></a>

### TryResult


| Name | Number | Description |
| ---- | ------ | ----------- |
| TRY_RESULT | 0 | try reult，该字段的名称在参数字段中为目标链的返回值占位 |



<a name="common-TxResultValue"></a>

### TxResultValue
tx执行结果

| Name | Number | Description |
| ---- | ------ | ----------- |
| TX_SUCCESS | 0 | 交易执行成功 |
| TX_TIMEOUT | 1 | 交易超时 |
| TX_FAIL | 2 | 交易失败 |
| TX_NOT_EXIST | 3 | 交易不存在 |
| TX_NO_PERMISSIONS | 4 | 交易权限不足 |
| GATEWAY_NOT_FOUND | 5 | 网关不存在 |
| GATEWAY_PINGPONG_ERROR | 6 | 目标网关验活错误 |
| CHAIN_PING_ERROR | 7 | 目标链验活错误 |
| SRC_GATEWAY_GET_ERROR | 8 | 源网关获取失败 |



<a name="common-TxVerifyRsult"></a>

### TxVerifyRsult
中继网关对交易的认证结果

| Name | Number | Description |
| ---- | ------ | ----------- |
| VERIFY_SUCCESS | 0 | 认证成功 |
| VERIFY_INVALID | 1 | 认证失败 |
| VERIFY_NOT_NEED | 2 | 不需要认证 |



<a name="common-TxVerifyType"></a>

### TxVerifyType


| Name | Number | Description |
| ---- | ------ | ----------- |
| SPV | 0 | spv方式认证 |
| RPC_INTERFACE | 1 | rpc接口认证 |
| NOT_NEED | 2 | 不需要认证 |



<a name="common-Version"></a>

### Version
版本号

| Name | Number | Description |
| ---- | ------ | ----------- |
| V1_0_0 | 0 | v1.0.0 |


 

 

 



<a name="common_cross_chain_request-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## common/cross_chain/request.proto
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0


<a name="cross_chain-ChainIdentityRequest"></a>

### ChainIdentityRequest
链配置管理


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| operate | [common.Operate](#common-Operate) |  | 操作 |
| chainmaker_config | [common.ChainmakerConfig](#common-ChainmakerConfig) |  | chainmaker配置，chainmaker网关专属 |
| fabric_config | [common.FabricConfig](#common-FabricConfig) |  | fabric配置，fabric网关专属 |
| bcos_config | [common.BcosConfig](#common-BcosConfig) |  | bcos配置，bcos网关专属 |






<a name="cross_chain-CrossChainCancelRequest"></a>

### CrossChainCancelRequest
跨链结果确认


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| cross_chain_id | [string](#string) |  | 跨链Id |
| cross_chain_name | [string](#string) |  | 跨链名称 |
| cross_chain_flag | [string](#string) |  | 跨链标记 |
| cancel_info | [common.CancelInfo](#common-CancelInfo) |  | cancel数据 |






<a name="cross_chain-CrossChainConfirmRequest"></a>

### CrossChainConfirmRequest
跨链结果确认


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| cross_chain_id | [string](#string) |  | 跨链Id |
| cross_chain_name | [string](#string) |  | 跨链名称 |
| cross_chain_flag | [string](#string) |  | 跨链标记 |
| confirm_info | [common.ConfirmInfo](#common-ConfirmInfo) |  | confirm数据 |
| try_result | [string](#string) | repeated | try_result |
| cross_type | [common.CrossType](#common-CrossType) |  | 跨链类型 |






<a name="cross_chain-CrossChainEventRequest"></a>

### CrossChainEventRequest
跨链事件管理请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| operate | [common.Operate](#common-Operate) |  | 操作 |
| cross_chain_event | [common.CrossChainEvent](#common-CrossChainEvent) |  | 具体内容 |






<a name="cross_chain-CrossChainTryRequest"></a>

### CrossChainTryRequest
中继网关向跨链网关发起跨链请求的消息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| cross_chain_id | [string](#string) |  | 跨链Id |
| cross_chain_name | [string](#string) |  | 跨链名称 |
| cross_chain_flag | [string](#string) |  | 跨链标记 |
| cross_chain_msg | [common.CrossChainMsg](#common-CrossChainMsg) |  | 跨链信息 |
| tx_content | [common.TxContentWithVerify](#common-TxContentWithVerify) |  | 跨链首笔交易内容 |
| from | [string](#string) |  | 发起跨链网关的id |






<a name="cross_chain-IsCrossChainSuccessRequest"></a>

### IsCrossChainSuccessRequest
判断跨链结果


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| cross_chain_id | [string](#string) |  | 跨链Id |
| cross_chain_name | [string](#string) |  | 跨链名称 |
| cross_chain_flag | [string](#string) |  | 跨链标记 |
| tx_content | [common.TxContentWithVerify](#common-TxContentWithVerify) | repeated | 每个网关执行跨链交易的结果 |






<a name="cross_chain-TxVerifyRequest"></a>

### TxVerifyRequest
交易验证请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| tx_prove | [string](#string) |  | 证明内容，json字符串，这里要和TxContent中的tx_prove字段对应 |





 

 

 

 



<a name="common_cross_chain_response-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## common/cross_chain/response.proto
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0


<a name="cross_chain-ChainIdentityResponse"></a>

### ChainIdentityResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| code | [common.Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |
| chainmaker_config | [common.ChainmakerConfig](#common-ChainmakerConfig) | repeated | chainmaker 配置列表 |
| fabric_config | [common.FabricConfig](#common-FabricConfig) | repeated | fabric 配置列表 |
| bcos_config | [common.BcosConfig](#common-BcosConfig) | repeated | bcos 配置列表 |






<a name="cross_chain-CrossChainCancelResponse"></a>

### CrossChainCancelResponse
跨链结果确认


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tx_content | [common.TxContent](#common-TxContent) |  | 交易详情，可选，不验证 |
| code | [common.Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |






<a name="cross_chain-CrossChainConfirmResponse"></a>

### CrossChainConfirmResponse
跨链结果确认


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tx_content | [common.TxContent](#common-TxContent) |  | 交易详情，可选，不验证 |
| code | [common.Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |






<a name="cross_chain-CrossChainEventResponse"></a>

### CrossChainEventResponse
跨链事件管理返回


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| code | [common.Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |
| cross_chain_event_list | [common.CrossChainEvent](#common-CrossChainEvent) | repeated | 事件列表 |






<a name="cross_chain-CrossChainTryResponse"></a>

### CrossChainTryResponse
中继网关向跨链网关发起跨链请求回复


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| cross_chain_id | [string](#string) |  | 跨链Id |
| cross_chain_name | [string](#string) |  | 跨链名称 |
| cross_chain_flag | [string](#string) |  | 跨链标记 |
| tx_content | [common.TxContent](#common-TxContent) |  | 跨链交易内容 |
| try_result | [string](#string) | repeated | 跨链交易结果 |
| code | [common.Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |






<a name="cross_chain-IsCrossChainSuccessResponse"></a>

### IsCrossChainSuccessResponse
判断跨链结果


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| cross_chain_result | [bool](#bool) |  | 跨链结果 |
| code | [common.Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |






<a name="cross_chain-PingPongResponse"></a>

### PingPongResponse
pingpong返回


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| chain_ok | [bool](#bool) |  | 链是否正常 |






<a name="cross_chain-TxVerifyResponse"></a>

### TxVerifyResponse
交易验证结果


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tx_verify_result | [bool](#bool) |  | 交易验证结果 |
| code | [common.Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |





 

 

 

 



<a name="common_relay_chain_request-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## common/relay_chain/request.proto
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0


<a name="relay_chain-BeginCrossChainRequest"></a>

### BeginCrossChainRequest
跨链网关向中继网关发送跨链请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| cross_chain_id | [string](#string) |  | 跨链Id，可以由网关生成 |
| cross_chain_name | [string](#string) |  | 跨链名称 |
| cross_chain_flag | [string](#string) |  | 跨链标记 |
| cross_chain_msg | [common.CrossChainMsg](#common-CrossChainMsg) | repeated | 跨链信息 |
| tx_content | [common.TxContent](#common-TxContent) |  | 跨链首笔交易内容 |
| from | [string](#string) |  | 发起跨链网关的id |
| timeout | [int64](#int64) |  | timeout, s |
| confirm_info | [common.ConfirmInfo](#common-ConfirmInfo) |  | confirm数据 |
| cancel_info | [common.CancelInfo](#common-CancelInfo) |  | cancel数据 |
| cross_type | [common.CrossType](#common-CrossType) |  | 跨链类型 |






<a name="relay_chain-GatewayRegisterRequest"></a>

### GatewayRegisterRequest
网关注册


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| gateway_info | [common.GatewayInfo](#common-GatewayInfo) |  | 网关信息 |






<a name="relay_chain-GatewayUpdateRequest"></a>

### GatewayUpdateRequest
网关更新


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| gateway_info | [common.GatewayInfo](#common-GatewayInfo) |  | 网关信息 |






<a name="relay_chain-InitContractRequest"></a>

### InitContractRequest
创建具有同步区块头、交易验证功能的spv合约的请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| contract_name | [string](#string) |  | 合约名，spv合约参数和名称是固定好的，不需要传，格式为&#34;spv[gatewayId][chainRid]&#34; |
| contract_version | [string](#string) |  | 版本号 |
| byte_code | [bytes](#bytes) |  | bytecode base64 |
| runtime_type | [common.ChainmakerRuntimeType](#common-ChainmakerRuntimeType) |  | runtimeType |
| key_value_pairs | [common.ContractKeyValuePair](#common-ContractKeyValuePair) | repeated | 初始化参数 |
| gateway_id | [string](#string) |  | 网关id |
| chain_rid | [string](#string) |  | 链资源id |






<a name="relay_chain-QueryCrossChainRequest"></a>

### QueryCrossChainRequest
查询跨链信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| cross_chain_id | [string](#string) |  | 跨链id（可选） |
| page_size | [uint64](#uint64) |  | 每页多少数据(可选) |
| page_number | [uint64](#uint64) |  | 第几页(可选) |






<a name="relay_chain-QueryGatewayRequest"></a>

### QueryGatewayRequest
查询网关信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| gateway_id | [string](#string) |  | 网关id（可选） |
| page_size | [uint64](#uint64) |  | 每页多少数据(可选) |
| page_number | [uint64](#uint64) |  | 第几页(可选) |






<a name="relay_chain-SyncBlockHeaderRequest"></a>

### SyncBlockHeaderRequest
向中继网关发送同步区块请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| gateway_id | [string](#string) |  | 区块头与网关Id |
| chain_rid | [string](#string) |  | 链id |
| block_height | [uint64](#uint64) |  | 区块高度 |
| block_header | [bytes](#bytes) |  | 区块头内容 |






<a name="relay_chain-UpdateContractRequest"></a>

### UpdateContractRequest
更新具有同步区块头、交易验证功能的spv合约的请求


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [common.Version](#common-Version) |  | 请求消息版本 |
| contract_name | [string](#string) |  | 合约名 |
| contract_version | [string](#string) |  | 版本号 |
| byte_code | [bytes](#bytes) |  | bytecode |
| runtime_type | [common.ChainmakerRuntimeType](#common-ChainmakerRuntimeType) |  | runtimeType |
| key_value_pairs | [common.ContractKeyValuePair](#common-ContractKeyValuePair) | repeated | 初始化参数 |
| gateway_id | [string](#string) |  | 网关id |
| chain_rid | [string](#string) |  | 链资源id |





 

 

 

 



<a name="common_relay_chain_response-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## common/relay_chain/response.proto
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0


<a name="relay_chain-BeginCrossChainResponse"></a>

### BeginCrossChainResponse
跨链网关向中继网关发送跨链请求返回


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| code | [common.Code](#common-Code) |  | 执行结果code |
| cross_chain_id | [string](#string) |  | 跨链Id |
| message | [string](#string) |  | 返回消息内容 |






<a name="relay_chain-GatewayRegisterResponse"></a>

### GatewayRegisterResponse
网关注册


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| code | [common.Code](#common-Code) |  | 执行结果code |
| gateway_id | [string](#string) |  | 跨链网关Id |
| message | [string](#string) |  | 返回消息内容 |






<a name="relay_chain-GatewayUpdateResponse"></a>

### GatewayUpdateResponse
网关更新


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| code | [common.Code](#common-Code) |  | 执行结果code |
| gateway_id | [string](#string) |  | 跨链Id |
| message | [string](#string) |  | 返回消息内容 |






<a name="relay_chain-InitContractResponse"></a>

### InitContractResponse
安装区块头同步和spv验证合约


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| code | [common.Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |






<a name="relay_chain-QueryCrossChainResponse"></a>

### QueryCrossChainResponse
查询跨链信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| cross_chain_info | [common.CrossChainInfo](#common-CrossChainInfo) | repeated | 跨链信息列表 |
| page_info | [common.PageInfo](#common-PageInfo) |  | 分页信息 |
| code | [common.Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |






<a name="relay_chain-QueryGatewayResponse"></a>

### QueryGatewayResponse
查询网关信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| gateway_info | [common.GatewayInfo](#common-GatewayInfo) | repeated | 网关信息列表 |
| page_info | [common.PageInfo](#common-PageInfo) |  | 分页信息 |
| code | [common.Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |






<a name="relay_chain-SyncBlockHeaderResponse"></a>

### SyncBlockHeaderResponse
向中继网关发送同步区块请求返回


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| code | [common.Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |






<a name="relay_chain-UpdateContractResponse"></a>

### UpdateContractResponse
升级区块头同步和spv验证合约


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| code | [common.Code](#common-Code) |  | 执行结果code |
| message | [string](#string) |  | 返回消息内容 |





 

 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

