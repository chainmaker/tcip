#!/bin/bash

cd ..
rm -rf tcip-go
git clone git@git.code.tencent.com:ChainMaker/tcip-go.git
cd tcip-go
git checkout -B "$1"
git pull origin "$1"
git clean -d -fx
cd "$3"
set -e
make pb rpc
cd ../tcip-go
go mod tidy
git add .
git commit -am "$2"
git push --set-upstream origin "$1"
